import { Template } from '@angular/compiler/src/render3/r3_ast';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css'],
  template: `<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Consequuntur est molestias omnis harum quaerat aliquam voluptates hic perspiciatis, in corporis? Laboriosam fugiat accusantium aliquid enim dicta? Aliquam sunt eveniet temporibus.</p>`
})
export class BodyComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
